### Ferramentas utilizadas
- React Native CLI
- Postgres
- Swagger
- Android Studio
- Heroku
- Visual Studio Code



### Como instalar o React native cli quickstart

Seguir o tutorial do react native quickstart
[Links](https://reactnative.dev/docs/environment-setup)

### Como instalar o Back end
- Clonar o projeto do repositorio
`<link>` : <https://gitlab.com/gleisson.je7/delta-back-end.git>
- Abrir no visual studio code e dar o seguinte comando no terminal
`npm i`
- Dar esse outro comando no terminal 
`npm start`

### Como instalar o Front end
- Clonar o projeto do repositorio
`<link>` : <https://gitlab.com/gleisson.je7/delta-front-end.git>
- Abrir no visual studio code e dar o seguinte comando no terminal
`npm i`
- Dar esse outro comando no terminal 
`npx react-native start`
- Abrir um novo terminal, navegar até a pasta raiz e dar o seguinte comando 
`npx react-native run-android`

Obs: Se tudo der certo o app deve abrir no emulador da seguinte da maneira 
[Link da imagem](https://drive.google.com/file/d/1xjPoZeWKPm6uwM0XCTStXpkugdyuRc3A/view?usp=sharing)




