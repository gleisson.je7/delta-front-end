import React from "react";
import { View, Text } from "react-native";
import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';



import Home from './src/Home';
import Cadastro from './src/Cadastro';
import Deletar from './src/Deletar';
import Visualizar from './src/Visualizar';
import Atualizar from './src/Atualizar';

const Stack = createStackNavigator();

export default function App(){
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Cadastro" component={Cadastro} />
        <Stack.Screen name="Deletar" component={Deletar} />
        <Stack.Screen name="Visualizar" component={Visualizar} />
        <Stack.Screen name="Atualizar" component={Atualizar} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}