import React, { Component } from 'react';

import { Alert, Text, StyleSheet, View, Image, TextInput, Buffer, ToastAndroid, TouchableOpacity } from 'react-native';
import axios, { put } from 'axios';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob'

const Cadastrar = () => {
  const [nome, onChangeText] = React.useState('');
  const [endereco, onChangeText2] = React.useState('');
  const [fonte, setSource] = React.useState(null);
  const [type, setType] = React.useState(null);

  const CadastrarUsuario = () => {
    RNFetchBlob.fetch('PUT', 'http://10.0.2.2:3000/user', {
      'Content-Type': 'multipart/form-data',
    }, [
      { name: 'foto', filename: 'avatar.png', data: fonte, type: type },
      { name: 'nome', data: nome },
      { name: 'endereco', data: endereco }
    ]).then((resp) => {
      ToastAndroid.show("usuario cadastrado", ToastAndroid.LONG)
    }).catch((err) => {
    })
  }

  var options = {
    title: 'Selecione a imagem',

    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const image = () => {
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setType(response.type);
        setSource(response.data);
      }
    });
  }

  return (<View>
    <Image source={{ uri: `data:image/gif;base64,${fonte}` }} style={{ width: 300, height: 300 }} />
    <TextInput
      style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
      onChangeText={text => onChangeText(text)}
      value={nome} />

    <TextInput
      style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
      onChangeText={text => onChangeText2(text)}
      value={endereco} />

    <TouchableOpacity
      style={styles.botao}
      onPress={() => { image() }}>
      <Text style={styles.botaoText}>Upload foto</Text>
    </TouchableOpacity>

    <TouchableOpacity
      style={styles.botao}
      onPress={() => { CadastrarUsuario() }}>
      <Text style={styles.botaoText}>Cadastrar</Text>
    </TouchableOpacity>
  </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50'
  },
  input: {
    marginTop: 10,
    padding: 10,
    width: 300,
    backgroundColor: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    borderRadius: 3
  },
  botao: {
    width: 300,
    height: 42,
    backgroundColor: '#3498db',
    marginTop: 10,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center'
  },
  botaoText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff'
  },
  titulo: {
    color: '#fff'
  }
})
export default Cadastrar;