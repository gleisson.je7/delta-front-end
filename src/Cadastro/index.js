import React from "react";
import { Alert, Text, StyleSheet, View, TextInput, TouchableOpacity } from 'react-native'
import Cadastrar from './Cadastrar'


export default function Cadastro() {
    return (
        <View style={styles.container}>
            <Text style={styles.titulo}>CRUD DELTA</Text>
            <Cadastrar></Cadastrar>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50'
    },
    input: {
        marginTop: 10,
        padding: 10,
        width: 300,
        backgroundColor: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 3
    },
    botao: {
        width: 300,
        height: 42,
        backgroundColor: '#3498db',
        marginTop: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    botaoText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    titulo: {
        color: '#fff'
    }
})