import axios, { get } from 'axios';
import { Alert, Text, StyleSheet, View, TextInput, Image, TouchableOpacity} from 'react-native'
import React, { useState } from 'react';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob'

export default function Atualizar({route, navigation}) {
    let itemParam = route.params;

    const [type , setType] = useState("itemParam");
    const [imagem, setImagem] = useState(itemParam.imagem);
    const [nome, setNome] = useState(itemParam.nome);
    const [endereco, setEndereco] = useState(itemParam.endereco);

    const atualizarUsuario = () => {
        RNFetchBlob.fetch('POST', 'http://10.0.2.2:3000/user/'+itemParam.id, {
       'Content-Type' : 'multipart/form-data',
     }, [
       { name : 'foto', filename : 'avatar.png', data: imagem, type:type},
       { name : 'nome', data:nome },
       { name : 'endereco', data: endereco}
     ]).then((resp) => {
       ToastAndroid.show("usuario atualizado",ToastAndroid.LONG)
     }).catch((err) => {
    })
   }

   var options = {
        title: 'Selecione a imagem',
        
        storageOptions: {
        skipBackup: true,
        path: 'images',
        },
    };

   const image = () => {
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }  else {
        setType(response.type);
        setImagem(response.data);
       }
    });
   }

    return (
        <View style={styles.container}>
            <Text style={styles.titulo}>CRUD DELTA</Text>
            <Image source={{uri:`data:image/gif;base64,${imagem}`}} style={{width: 300, height: 300}} />
            <TextInput
                style={styles.input}
                value = {nome}
                onChangeText={text => setNome(text)}/>

            <TextInput
                style={styles.input}
                value = {endereco}
                onChangeText={text => setEndereco(text)}/>

            <TouchableOpacity
                style={styles.botao}
                onPress={()=> {image()} }>
                <Text style={styles.botaoText}>Upload foto</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.botao}
                onPress={()=> {atualizarUsuario()} }>
                <Text style={styles.botaoText}>Atualizar</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50'
    },
    input: {
        marginTop: 10,
        padding: 10,
        width: 300,
        backgroundColor: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 3
    },
    botao: {
        width: 300,
        height: 42,
        backgroundColor: '#3498db',
        marginTop: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    botaoText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    titulo: {
        color: '#fff'
    }
})