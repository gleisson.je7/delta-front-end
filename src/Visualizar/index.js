import React from "react";
import { Alert, Text, StyleSheet, View, TextInput, FlatList, TouchableOpacity } from 'react-native'
import VisualizarUsuarios from './Visualizar';

export default function Visualizar({ navigation }) {
    return (
        <View style={styles.container}>
            <VisualizarUsuarios navigation={navigation}></VisualizarUsuarios>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50'
    },
    input: {
        marginTop: 10,
        padding: 10,
        width: 300,
        backgroundColor: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 3
    },
    botao: {
        width: 300,
        height: 42,
        backgroundColor: '#3498db',
        marginTop: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'

    },
    botaoText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    titulo: {
        color: '#fff'
    }
})