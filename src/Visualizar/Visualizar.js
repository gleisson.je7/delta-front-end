import React, { Component } from "react";
import axios, { get } from 'axios';
import { Alert, Text, StyleSheet, View, TextInput, Image, FlatList, TouchableOpacity, Button } from 'react-native'

class VisualizarUsuarios extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    goToDetailScreen = (item) => {
        this.props.navigation.navigate('Atualizar', item);
    }

    componentDidMount() {
        const url = 'http://10.0.2.2:3000/usuarios';
        axios.get(url)
            .then(function (response) {
                // handle success
                this.setState({
                    data: response.data
                })

            }.bind(this))
            .catch(function (error) {
                // handle error
                console.log(error);
            })
    }
    deletar = (id) => {
        var urlDeletar = 'http://10.0.2.2:3000/user/' + id
        axios.delete(urlDeletar)
            .then((response) => {
                this.setState({
                    data: this.state.data.filter(x => x.id != id)
                })
            })

    }
    render() {
        return (
            <View>
                <FlatList
                    data={this.state.data}

                    keyExtractor={item => item.id}
                    renderItem={({ item }) =>
                        <View>

                            <Text style={styles.titulo}>{item.nome}</Text>
                            <Text style={styles.titulo}>{item.endereco}</Text>
                            <Image source={{ uri: `data:image/gif;base64,${item.imagem}` }} style={{ width: 300, height: 300 }} />

                            <TouchableOpacity
                                style={styles.botao}
                                onPress={() => { this.deletar(item.id) }}>
                                <Text style={styles.botaoText}>Deletar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.botao}
                                onPress={() => this.goToDetailScreen(item)}>
                                <Text style={styles.botaoText}>Editar</Text>
                            </TouchableOpacity>
                        </View>}
                />
            </View>
        );
    }
}

export default VisualizarUsuarios;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50'
    },
    input: {
        marginTop: 10,
        padding: 10,
        width: 300,
        backgroundColor: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 3
    },
    botao: {
        width: 300,
        height: 42,
        backgroundColor: '#3498db',
        marginTop: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    botaoText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff'
    },
    titulo: {
        color: '#fff'
    }
})